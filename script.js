const getCube = Math.pow(2,3);
const message = `The cube of 2 is ${getCube}`;
console.log(message);

const address = [258,'Washington Ave NW','California',90011];
const [ houseNumber, street, state, zipCode ] = address;
console.log(`I live at ${houseNumber} ${street}, ${state} ${zipCode}`);

const animal = {
	name: 'Lolong',
	species:'saltwater crocodile',
	weight:1075,
	measurement:'20 ft 3 in'
};
console.log(`${animal.name} was a ${animal.species}. He weighed at ${animal.weight} kgs with a measurement of ${animal.measurement}.`);

const numbers = [1,2,3,4,5];
numbers.forEach((number) => {
	console.log(`${number}`);
});
const reduceNumber = [1,2,3,4,5]; 
const sum =	reduceNumber.reduce((total,num) =>{
	return total + num;
});
	console.log(sum);


class Dog {
	constructor(name,age,breed){
		this.name = name,
		this.age  = age,
		this.breed = breed
	}
}
const myDog = new Dog();
myDog.name = 'Frannkie';
myDog.age = 5;
myDog.breed = 'Miniature Dachshund';
console.log(myDog);
